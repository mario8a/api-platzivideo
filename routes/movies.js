const express = require('express');
const passport = require('passport');
const MoviesService = require('../services/movies');
const cacheResponse = require('../utils/cacheResponse');
const scopesValidationHandler = require('../utils/middleware/scopesValidationHandler');
const validationHandler = require('../utils/middleware/validationHandler');
const { 
   movieIdSchema, 
   createMovieSchema, 
   updateMovieSchema } = require('../utils/schema/movies');
const { SIXTY_MINUTES_IN_SECONDS, FIVE_MINUTES_IN_SECONDS } = require('../utils/time');

//JWT Strategy
require('../utils/auth/strategies/jwt');

// La responsabilidad de las rutas esa saber como recibe parametros y como se los envia a los servicios
function moviesApi(app) {
   const router = express.Router();
   app.use("/api/movies", router);

   const moviesService = new MoviesService();

   router.get(
      '/',
      passport.authenticate('jwt', { session: false }),
      scopesValidationHandler(['read:movies']),
      async function(req, res, next) {
        cacheResponse(res, FIVE_MINUTES_IN_SECONDS);
        const { tags } = req.query;
  
        try {
          const movies = await moviesService.getMovies({ tags });
  
          res.status(200).json({
            data: movies,
            message: 'movies listed'
          });
        } catch (err) {
          next(err);
        }
      }
    );

   router.get(
      "/:movieId",
      passport.authenticate('jwt', {session: false}),
      scopesValidationHandler(['read:movies']),
      validationHandler({movieId: movieIdSchema}, 'params'), 
      async function(req , res, next) {
      cacheResponse(res, SIXTY_MINUTES_IN_SECONDS);
      //Los parametros son cuando vienen en la URL
      const {movieId} = req.params;
      try {
         const movies = await moviesService.getMovie({movieId}); //Regresa la primer pelicula del mock
         res.status(200).json({
            data: movies,
            message: 'movie retrieved'
         });
      } catch (error) {
         next(error);
      }
   });

   router.post(
      '/',
      passport.authenticate('jwt', {session: false}),
      scopesValidationHandler(['create:movies']),
      validationHandler(createMovieSchema), async function(req, res, next) {
      const { body: movie } = req;
      try {
        const createdMovieId = await moviesService.createMovie({ movie });
         // console.log(createdMovieId)
        res.status(201).json({
          data: createdMovieId,
          message: 'movie created'
        });
      } catch (err) {
        next(err);
      }
    });

   router.put(
      "/:movieId",
      passport.authenticate('jwt', {session: false}),
      scopesValidationHandler(['update:movies']),
      validationHandler({movieId: movieIdSchema}, 'params'),
      validationHandler(updateMovieSchema), 
      async function(req , res, next) {
      const {movieId} = req.params;
      const { body: movie } = req;

      try {
         const updatedMovieId = await moviesService.updateMovie({movieId, movie})
         res.status(200).json({
            data: updatedMovieId,
            message: 'movie updated'
         });
      } catch (error) {
         next(error);
      }
   });

   router.delete(
      "/:movieId",
      passport.authenticate('jwt', {session: false}),
      scopesValidationHandler(['deleted:movies']),
      validationHandler({movieId: movieIdSchema}, 'params'),
      async function(req , res, next) {
      const {movieId} = req.params;
      try {
         const deleteMovieId = await moviesService.deleteMovie({movieId});
         
         res.status(200).json({
            data: deleteMovieId,
            message: 'movie deleted'
         });
      } catch (error) {
         next(error);
      }
   });
   
}

module.exports = moviesApi;