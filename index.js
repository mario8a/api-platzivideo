const express = require('express');
const helmet = require('helmet');
const app = express();
const cors = require('cors');
//config
const { config } = require('./config/index');
//utils
const { logErrors, errorHandler, wrapErrors } = require('./utils/middleware/errorHandlers');
const notFoundHandler = require('./utils/middleware/notFoundHandler');
//Routes
const authApi = require('./routes/auth');
const moviesApi = require('./routes/movies');
const userMoviesApi = require('./routes/userMovies');

//Middleware body-parser
app.use(express.json());
app.use(cors());
app.use(helmet());

//routes
authApi(app);
moviesApi(app);
userMoviesApi(app);
//Catch error 404
app.use(notFoundHandler);

//Middleware personalizado para errores
// Deben ir al final de las rutas
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);


app.listen(config.port, function() {
   console.log(`Listening http://localhost:${config.port}`)
});